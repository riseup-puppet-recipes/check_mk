define check_mk::agent::install_local($source=undef, $content=undef, $ensure='present') {
  include check_mk::agent::local_checks

  file {
    "/usr/lib/check_mk_agent/local/${name}" :
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      content => $content,
      source  => $source,
      require => [ File['/usr/lib/check_mk_agent/local' ], Class['check_mk::agent'] ];
  }
}
