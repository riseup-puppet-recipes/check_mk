class check_mk::server::collect_hosts {
  Check_mk::Host <<| |>> {
    notify => Exec['check_mk-refresh']
  }
}
